import org.json.JSONArray;
import org.json.JSONObject;

import java.awt.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class Main {
    public static void main(String[] args) throws Exception {
        String projectName = "TadzikT_swift-rpg";
        String metricKeys = "ncloc,statements,functions,classes,bugs,vulnerabilities,code_smells," +
                "duplicated_lines_density,sqale_index,comment_lines_density,complexity,cognitive_complexity";

        URL url = new URL("https://sonarcloud.io/api/measures/component?component=" + projectName + "&metricKeys="
                + metricKeys);
        HttpURLConnection conn = (HttpURLConnection) (url).openConnection();

        conn.setRequestMethod("GET");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setDoOutput(true);

        JSONObject data = new JSONObject(inputStreamToString(conn.getInputStream()));
        JSONArray measures = data.getJSONObject("component").getJSONArray("measures");
        Map<String, String> metricsValues = new HashMap<>();
        for (int i = 0; i < measures.length(); i++) {
            JSONObject jsonObject = measures.getJSONObject(i);
            metricsValues.put(jsonObject.getString("metric"), jsonObject.getString("value"));
        }

        Map<String, String> metrics = new LinkedHashMap<>();
        metrics.put("Liczba linii kodu", metricsValues.get("ncloc"));
        metrics.put("Liczba wyrażeń", metricsValues.get("statements"));
        metrics.put("Liczba funkcji", metricsValues.get("functions"));
        metrics.put("Liczba klas", metricsValues.get("classes"));
        metrics.put("Liczba bugów", metricsValues.get("bugs"));
        metrics.put("Liczba luk w zabezpieczeniach", metricsValues.get("vulnerabilities"));
        metrics.put("Liczba zapachów kodu", metricsValues.get("code_smells"));
        metrics.put("Procent zduplikowanego kodu", metricsValues.get("duplicated_lines_density") + "%");
        metrics.put("Dług techniczny w minutach", metricsValues.get("sqale_index"));
        metrics.put("Procent kodu, który zajmują komentarze", metricsValues.get("comment_lines_density") + "%");
        metrics.put("Poziom cyklicznego skomplikowania kodu", metricsValues.get("complexity"));
        metrics.put("Poziom kognitywnego skomplikowania kodu", metricsValues.get("cognitive_complexity"));

        Map<String, String> aggregatedMetrics = new LinkedHashMap<>();
        int bugs = Integer.parseInt(metricsValues.get("bugs"));
        int vulnerabilities = Integer.parseInt(metricsValues.get("vulnerabilities"));
        int codeSmells = Integer.parseInt(metricsValues.get("code_smells"));
        int ncloc = Integer.parseInt(metricsValues.get("ncloc"));
        float problemsPerLine = (bugs + vulnerabilities + codeSmells) / (float) ncloc;
        aggregatedMetrics.put("Liczba problemów kodu w przeliczeniu na liczbę linii", String.valueOf(problemsPerLine));

        int complexity = Integer.parseInt(metricsValues.get("complexity"));
        int cognitive_complexity = Integer.parseInt(metricsValues.get("cognitive_complexity"));
        float avgComplexity = (complexity + cognitive_complexity) / 2f;
        aggregatedMetrics.put("Ogólny poziom skomplikowania kodu", String.valueOf(avgComplexity));

        int classes = Integer.parseInt(metricsValues.get("classes"));
        float linesPerClass = ncloc / (float) classes;
        aggregatedMetrics.put("Średnia liczba linii kodu w jednej klasie", String.valueOf(linesPerClass));

        int sqale_index = Integer.parseInt(metricsValues.get("sqale_index"));
        float debtPerLines = sqale_index / (float) ncloc;
        aggregatedMetrics.put("Dług techniczny w przeliczeniu na liczbę linii kodu", String.valueOf(debtPerLines));

        Map<String, String> indicators = new LinkedHashMap<>();
        float codeQuality = 5000 - problemsPerLine * 20000 - avgComplexity - linesPerClass * 5 - debtPerLines * 2000;
        indicators.put("Ogólna jakość kodu (liczbowo)", String.valueOf(codeQuality));

        String codeQualityRating = "A";
        if (codeQuality < 3000f) {
            codeQualityRating = "B";
        } else if (codeQuality < 2000f) {
            codeQualityRating = "C";
        } else if (codeQuality < 1000f) {
            codeQualityRating = "D";
        } else if (codeQuality < 500f) {
            codeQualityRating = "E";
        } else if (codeQuality < 0f) {
            codeQualityRating = "F";
        }
        indicators.put("Ogólna jakość kodu (ocena w skali A-F)", codeQualityRating);

        File f = new File("web/page.html");
        BufferedWriter bw = new BufferedWriter(new FileWriter(f));
        bw.write("<!DOCTYPE html>\n" +
                 "<html>\n" +
                 "<head>\n" +
                 "<title>Swift Metrics</title>\n" +
                 "<meta charset=\"utf-8\">\n" +
                 "<link rel=\"icon\" href=\"favicon.ico\" type=\"image/x-icon\">\n" +
                 "<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\">\n" +
                 "</head>\n" +
                 "<body>\n" +
                 "<h1>Swift Metrics</h1>\n" +
                 "<h2>Nazwa projektu: " + projectName + "</h2>\n");
        writeTableFromMap(bw, "Metryki", metrics);
        writeTableFromMap(bw, "Zagregowane metryki", aggregatedMetrics);
        writeTableFromMap(bw, "Indykatory", indicators);
        bw.write("</body>\n" +
                 "</html>");
        bw.close();

        Desktop.getDesktop().browse(f.toURI());
    }

    private static String inputStreamToString(InputStream inputStream) throws IOException {
        try (ByteArrayOutputStream result = new ByteArrayOutputStream()) {
            byte[] buffer = new byte[1024];
            int length;
            while ((length = inputStream.read(buffer)) != -1) {
                result.write(buffer, 0, length);
            }
            return result.toString("UTF-8");
        }
    }

    private static void writeTableFromMap(BufferedWriter bw, String title, Map<String, String> map) throws IOException {
        bw.write("<h3>" + title + "</h3>\n" +
                 "<table class=\"table-fill\">\n" +
                 "  <thead>\n" +
                 "    <tr>\n" +
                 "      <th class=\"text-left\">Nazwa</th>\n" +
                 "      <th class=\"text-left\">Wartość</th>\n" +
                 "    </tr>\n" +
                 "  </thead>\n" +
                 "  <tbody class=\"table-hover\">\n");
        for (String key : map.keySet()) {
            bw.write("    <tr>\n" +
                     "      <td class=\"text-left\">" + key + "</td>\n" +
                     "      <td class=\"text-left\">" + map.get(key) + "</td>\n" +
                     "    </tr>\n");
        }
        bw.write("  </tbody>\n" +
                 "</table>\n");
    }
}
